const jwt = require ('jsonwebtoken');
const secret = 'Nitrosphere';

module.exports.createAccessToken = (user) => {
    console.log(user);
    const data = {
        id : user._id,
        email : user.email,
        isAdmin : user.isAdmin
    }
    console.log(data)
    return jwt.sign(data, secret, {})
};


module.exports.verify = (req,res,next) => {
    let token = req.headers.authorization;
    if (typeof token === 'undefined'){
        return res.send ({auth: 'Failed. No Token'})
    } else {
        token = token.slice(7, token.length)

        jwt.verify(token, secret, (err, decodedToken) =>{
            if (err){
                res.send({
                    auth : "Failed",
                    message : err.message
                })
            } else {
                req.user = decodedToken,
                next ();
            }
        })

    }
};


module.exports.verifyAdmin = (req,res,next) => {
    if(req.user.isAdmin){
        next();
    } else {
        return res.send(
            `ACTION FORBIDDEN`
        )
    }
};